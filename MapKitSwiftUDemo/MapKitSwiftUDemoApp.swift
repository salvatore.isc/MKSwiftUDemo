//
//  MapKitSwiftUDemoApp.swift
//  MapKitSwiftUDemo
//
//  Created by Salvador Lopez on 14/06/23.
//

import SwiftUI

@main
struct MapKitSwiftUDemoApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
