//
//  ContentView.swift
//  MapKitSwiftUDemo
//
//  Created by Salvador Lopez on 14/06/23.
//

import SwiftUI
import MapKit

struct MapView: UIViewRepresentable {
    
    func updateUIView(_ uiView: MKMapView, context: Context) {
        
        //Region
        //@6.2552714,-75.5917301
        let coordinate = CLLocationCoordinate2D(latitude: 6.2552714, longitude: -75.5917301)
        let span = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
        let region = MKCoordinateRegion(center: coordinate, span: span)
        uiView.setRegion(region, animated: true)
        
        //Annotation
        let annotation = MKPointAnnotation()
        annotation.title = "Atanasio Girardot Stadium"
        annotation.subtitle = "Medellin - Colombia"
        annotation.coordinate = coordinate
        uiView.addAnnotation(annotation)
        uiView.selectAnnotation(annotation, animated: true)
    }
    
    func makeUIView(context: Context) -> MKMapView {
        MKMapView(frame: .zero)
    }

}

struct ContentView: View{
    var body: some View{
        MapView()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
